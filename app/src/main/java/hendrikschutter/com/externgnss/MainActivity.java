package hendrikschutter.com.externgnss;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.location.LocationListener;
import android.location.LocationManager;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

import static java.lang.Thread.sleep;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MainActivity extends AppCompatActivity {
    private TextView output;
    private LocationManager locationManager = null;
    private SerialUSB serialUSB = null;
    private NMEAParser nmeaParser = null;
    private boolean doRun = true;
    private final Context cntxToastInternGNSSUpdate = this;
    final String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET,};
    private static final String INTENT_ACTION_GRANT_USB = BuildConfig.APPLICATION_ID + ".GRANT_USB";
    private boolean bChange = true;
    private BlockingQueue<Byte> rawSerialByteDataQueue = null;
    private double lastInternFixLat, lastInternFixLon;
    private MainActivity activity = this;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // start intern GNSS and display stats on UI
        startInternGNSS();

        // start extern GNSS and display stats on UI
        startExternGNSS();
    }

    // From https://github.com/googlesamples/easypermissions
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    private void startExternGNSS() {
        rawSerialByteDataQueue = new LinkedBlockingQueue<>();
        serialUSB = new SerialUSB(this, rawSerialByteDataQueue);
        nmeaParser = new NMEAParser();

        if (serialUSB.findSerialDevice()) {
            serialUSB.connect(9600, 0);
            if (serialUSB.isConnected()) {
                nmeaParser.setReceiveByteStream(rawSerialByteDataQueue);

                ((FloatingActionButton)findViewById(R.id.floatingActionButtonExternGnssInfo)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String otgInfo = "OTG-Info:\n  Vendor: " + serialUSB.getUsbDeviceVendorId()+ "" +
                                "\n  Device: " + serialUSB.getDeviceId() +
                                "\n  Baudrate: " + String.valueOf(serialUSB.getBaudRate() +
                                "\n\nNMEA:\n  Sentences: " + String.valueOf(nmeaParser.getSentenceCounter()));
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setMessage(otgInfo)
                                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                });
                        builder.create().show();
                    }
                });
            }
        }
    }

    @AfterPermissionGranted(123)
    private void startInternGNSS() {
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Already have permission, do the thing
            Toast.makeText(cntxToastInternGNSSUpdate, "Permission granted!", Toast.LENGTH_SHORT).show();
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "We require location access!",
                    123, perms);
        }
    }


    private void setEditTextLabel(final EditText v, final String str) {
        Runnable myRun = new Runnable() {
            public void run() {
                v.setText(str);
            }
        };
        runOnUiThread(myRun);
    }

    private void setTextViewLabel(final TextView v, final String str) {
        Runnable myRun = new Runnable() {
            public void run() {
                v.setText(str);
            }
        };
        runOnUiThread(myRun);
    }



    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();
        setInternGnssUiNoSignal();
        setExternGnssUiNoSignal();
        setInternGnssUiColor(Color.GRAY);
        setExternGnssUiColor(Color.GRAY);

        if (!EasyPermissions.hasPermissions(this, perms)) {
            // Dummy thread ...
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (doRun) {
                        Log.i("HAG", "Just do nothing ...");
                        try {
                            sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            // Worker-Thread
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            while (doRun) {
                                if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
                                    setInternGnssUiColor(Color.BLACK);
                                    lastInternFixLat = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude();
                                    lastInternFixLon = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude();
                                    setEditTextLabel((EditText) findViewById(R.id.interngnssLatNumber), String.format("%.6f", lastInternFixLat));
                                    setEditTextLabel((EditText) findViewById(R.id.interngnssLongNumber), String.format("%.6f", lastInternFixLon));
                                    setEditTextLabel((EditText) findViewById(R.id.interngnssAltitudeNumber), String.format("%.2f", locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getAltitude()) + " m");
                                    setEditTextLabel((EditText) findViewById(R.id.interngnssAccuracyNumber), String.format("%.2f", locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getAccuracy()) + " m");
                                    setEditTextLabel((EditText) findViewById(R.id.interngnssSatCountNumber), Integer.toString(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getExtras().getInt("satellites")));

                                } else {
                                    setInternGnssUiColor(Color.GRAY);
                                    setInternGnssUiNoSignal();
                                    setEditTextLabel((EditText) findViewById(R.id.diffGNSSDistanceNumber), "no data");
                                }

                                if ((serialUSB != null) && (nmeaParser != null)) {
                                    if (serialUSB.isConnected()) {
                                        setTextViewLabel((TextView) findViewById(R.id.externgnssNameTextView), "Extern GNSS receiver");
                                        nmeaParser.handleReceiveByteStream();
                                        if (nmeaParser.checkFix() == true) {
                                            setExternGnssUiColor(Color.BLACK);
                                            //Log.i("ExternGNSS", "Fix! Lat: " + String.valueOf(nmeaParser.getLatitude()) + " Lon: " + String.valueOf(nmeaParser.getLongitude()));
                                            setEditTextLabel((EditText) findViewById(R.id.externgnssLatNumber), String.format("%.6f", nmeaParser.getLatitude()));
                                            setEditTextLabel((EditText) findViewById(R.id.externgnssLongNumber), String.format("%.6f", nmeaParser.getLongitude()));
                                            setEditTextLabel((EditText) findViewById(R.id.externgnssSatCountNumber), Integer.toString(nmeaParser.getSatCount()));
                                        }
                                    } else {
                                        setTextViewLabel((TextView) findViewById(R.id.externgnssNameTextView), "Extern GNSS receiver (offline)");
                                        setExternGnssUiColor(Color.GRAY);
                                        setExternGnssUiNoSignal();
                                        setEditTextLabel((EditText) findViewById(R.id.diffGNSSDistanceNumber), "no data");
                                    }

                                    if ((locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) && (serialUSB != null) && (nmeaParser != null)) {
                                        if (nmeaParser.checkFix() == true) {
                                            setEditTextLabel((EditText) findViewById(R.id.diffGNSSDistanceNumber), String.format("%.6f", nmeaParser.calcDistance(lastInternFixLat, lastInternFixLon)) + " m");
                                        }
                                    } else {
                                        setEditTextLabel((EditText) findViewById(R.id.diffGNSSDistanceNumber), "no signal");
                                    }
                                }
                                try {
                                    sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }).start();
        }
    }

    /**
     * Stop the updates when Activity is paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        doRun = false;
    }

    private void setInternGnssUiColor(int color){
        ((TextView) findViewById(R.id.interngnssLatTextView)).setTextColor(color);
        ((TextView) findViewById(R.id.interngnssLongTextView)).setTextColor(color);
        ((TextView) findViewById(R.id.interngnssAccuracyTextView)).setTextColor(color);
        ((TextView) findViewById(R.id.interngnssAltitudeTextView)).setTextColor(color);
        ((TextView) findViewById(R.id.interngnssSatCountTextView)).setTextColor(color);

        ((EditText) findViewById(R.id.interngnssLatNumber)).setTextColor(color);
        ((EditText) findViewById(R.id.interngnssLongNumber)).setTextColor(color);
        ((EditText) findViewById(R.id.interngnssAccuracyNumber)).setTextColor(color);
        ((EditText) findViewById(R.id.interngnssAltitudeNumber)).setTextColor(color);
        ((EditText) findViewById(R.id.interngnssSatCountNumber)).setTextColor(color);
    }

    private void setExternGnssUiColor(int color){
        ((TextView) findViewById(R.id.externgnssLatTextView)).setTextColor(color);
        ((TextView) findViewById(R.id.externgnssLongTextView)).setTextColor(color);
        ((TextView) findViewById(R.id.externgnssSatCountTextView)).setTextColor(color);

        ((EditText) findViewById(R.id.externgnssLatNumber)).setTextColor(color);
        ((EditText) findViewById(R.id.externgnssLongNumber)).setTextColor(color);
        ((EditText) findViewById(R.id.externgnssSatCountNumber)).setTextColor(color);
    }

    private void setInternGnssUiNoSignal(){
        setEditTextLabel((EditText) findViewById(R.id.interngnssLatNumber), "no signal");
        setEditTextLabel((EditText) findViewById(R.id.interngnssLongNumber), "no signal");
        setEditTextLabel((EditText) findViewById(R.id.interngnssAltitudeNumber), "no signal");
        setEditTextLabel((EditText) findViewById(R.id.interngnssAccuracyNumber), "no signal");
        setEditTextLabel((EditText) findViewById(R.id.interngnssSatCountNumber), "no signal");
    }

    private void setExternGnssUiNoSignal(){
        setEditTextLabel((EditText) findViewById(R.id.externgnssLatNumber), "no signal");
        setEditTextLabel((EditText) findViewById(R.id.externgnssLongNumber), "no signal");
        setEditTextLabel((EditText) findViewById(R.id.externgnssSatCountNumber), "no signal");
        setEditTextLabel((EditText) findViewById(R.id.diffGNSSDistanceNumber), "no signal");
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            Toast.makeText(cntxToastInternGNSSUpdate, "Internal GNSS updated", Toast.LENGTH_LONG).show();
        }

        @Override
        // This callback will never be invoked on Android Q and above.
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // Toast.makeText(mContext, "Status Changed", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };
}
