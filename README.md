# ExternGnss

Display smartphone GNSS data and external GNSS from a serial module via OTG. Show distance between the both fixes. 

<img src="https://git.mosad.xyz/localhorst/ExternGnss/raw/commit/a3b6942042d46e244439895a88e1d3977fae371f/Screenshot_20220425-213544_ExternGNSS.png" height="500">

## Used Libs
[usb-serial-for-android](https://github.com/mik3y/usb-serial-for-android) as the OTG serial driver.

## External GNSS module
For testing a Ublox NEO 8 was used with a FTDI FT232R. Just connect the TX/RX pairs and select 5V for the module.
- [FTDI FT232R](https://www.ebay.com/itm/165323481324)
- [Ublox NEO 8 GNSS](https://de.aliexpress.com/item/4000597350406.html)

## Tip for USB-free ADB connection
- connect the device via USB
- set the adb server to network debugging: `adb tcpip 5555`
- get the device IP address: `adb shell ip addr show wlan0`
- connecy the device via network: `adb connect ip-address-of-device:5555`
- verify the network connection: `adb devices`
- unplug the USB cable from device
- the device should still be attached via network to adb
